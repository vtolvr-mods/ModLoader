param (
    [Parameter(Mandatory = $true )]
    [string]$ModLoaderFolder,

    [Parameter(Mandatory = $true )]
    [string]$VTOLVRFolder,

    [Parameter(Mandatory = $true )]
    [string]$SteamExePath
)

Write-Output "Publishing Mod Loader"
dotnet publish "$ModLoaderFolder\ModLoader\Mod Loader.csproj" --nologo --configuration Release -consoleLoggerParameters:ErrorsOnly

Write-Output "Deleting Managed Folder"
Get-ChildItem -Path "$VTOLVRFolder\@Mod Loader\Managed" -Include *.* -File -Recurse | foreach {
    $_.Delete()
}

Write-Output "Moving new files into Managed folder"

$gameItems = Get-ChildItem -Path "$VTOLVRFolder\VTOLVR_Data\Managed" -Name
$myItems = Get-ChildItem -Path "$ModLoaderFolder\ModLoader\bin\Release\netstandard2.0\publish" -Name
$itemsToMove = New-Object System.Collections.Generic.List[System.String]

foreach ($item in $myItems)
{
    if ($gameItems -notcontains $item)
    {
        $itemsToMove.Add($item)
    }
}

foreach ($item in $itemsToMove)
{
    Copy-Item "$ModLoaderFolder\ModLoader\bin\Release\netstandard2.0\publish\$item" -Destination "$VTOLVRFolder\@Mod Loader\Managed\$item"
}

Write-Output "Publishing SteamQueries"

dotnet publish "$ModLoaderFolder\SteamQueries\SteamQueries.csproj" --nologo --configuration Release -consoleLoggerParameters:ErrorsOnly

Get-ChildItem -Path "$VTOLVRFolder\@Mod Loader\SteamQueries" -Include *.* -File -Recurse | foreach {
    $_.Delete()
}

Write-Output "Moving SteamQueries to game folder"
Get-ChildItem -Path "$ModLoaderFolder\SteamQueries\bin\Release\net7.0\publish" -Include *.* -File -Recurse | foreach {
    $_.CopyTo("$VTOLVRFolder\@Mod Loader\SteamQueries\" + $_.Name) 
}

Write-Output "Moving Asset Bundle"

$assetsGamePath = "$VTOLVRFolder\@Mod Loader\assets"
$assetsUnityPath = "$ModLoaderFolder\Asset Bundle Project\Assets\@Mod Loader\Output\assets"

$assetsGamePathExists = Test-Path $assetsGamePath
$assetsUnityPathExists = Test-Path $assetsUnityPath
if ($assetsUnityPathExists)
{
    if ($assetsGamePathExists)
    {
        Remove-Item $assetsGamePath
    }
    
    Copy-Item $assetsUnityPath -Destination $assetsGamePath
    Write-Output "Copied Asset Bundle from Unity to game files"
}

$assetsGamePathExists = Test-Path $assetsGamePath
if (-not $assetsGamePathExists)
{
    Write-Error "The Asset bundle doesn't exist!!!"
}

Start-Process "$SteamExePath" -Args "-applaunch 667970 --doorstop-enabled true"