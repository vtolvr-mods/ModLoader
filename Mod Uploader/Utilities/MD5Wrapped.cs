﻿using System.IO;
using System.Security.Cryptography;
using Mod_Uploader.Abstractions;

namespace Mod_Uploader.Utilities;

public class MD5Wrapped : IMD5
{
    private MD5 _md5;

    public MD5Wrapped()
    {
        _md5 = MD5.Create();
    }

    public byte[] ComputeHash(Stream inputStream) => _md5.ComputeHash(inputStream);
}