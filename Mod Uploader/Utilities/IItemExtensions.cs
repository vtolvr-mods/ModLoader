﻿using System;
using Facepunch.Steamworks.Abstractions;
using Newtonsoft.Json;
using SteamQueries.Models;

namespace Mod_Uploader.Utilities;

internal static class IItemExtensions
{
    public static ItemMetaData GetMetaData(this IItem item)
    {
        if (string.IsNullOrWhiteSpace(item.MetaData))
        {
            throw new ArgumentNullException(nameof(item.MetaData), "Metadata is empty on item");
        }
        
        return JsonConvert.DeserializeObject<ItemMetaData>(item.MetaData);
    }
}