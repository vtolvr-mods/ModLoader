﻿using Facepunch.Steamworks.Abstractions;
using Mod_Uploader.Abstractions;
using Serilog;

namespace Mod_Uploader.Models;

public class Context : IContext
{
    public event SteamItemScopeChanged? SteamItemScopeChanged;
    private IItem? _currentItem;
    
    public void ChangeSteamItemScope(object sender, IItem? newSteamItem)
    {
        _currentItem = newSteamItem;
        Log.Verbose("Steam Item Scope changed to {ItemId}", newSteamItem?.Id?.Value);
        SteamItemScopeChanged?.Invoke(sender, _currentItem);
    }

    public IItem? GetSteamItemScope() => _currentItem;
}