using System.Collections.Generic;
using System.Linq;
using ReactiveUI;

namespace Mod_Uploader.Models;

public class SelectedTags : ReactiveObject
{
    public List<WorkshopTag> Tags { get; private set; }= new();
    private string _allTags;
    public string AllTags
    {
        get => _allTags;
        set => this.RaiseAndSetIfChanged(ref _allTags, value); 
    }
    
    public void Add(WorkshopTag tag)
    {
        Tags.Add(tag);
        AllTags = string.Join(',', Tags.Select(t => t.Name));
    }

    public bool Remove(WorkshopTag tag)
    {
        var result = Tags.Remove(tag);
        AllTags = string.Join(',', Tags.Select(t => t.Name));
        return result;
    }

    public void Clear()
    {
        Tags.Clear();
    }
}