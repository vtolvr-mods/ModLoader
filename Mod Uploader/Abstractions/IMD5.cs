﻿using System.IO;

namespace Mod_Uploader.Abstractions;

public interface IMD5
{
    byte[] ComputeHash(Stream inputStream);
}