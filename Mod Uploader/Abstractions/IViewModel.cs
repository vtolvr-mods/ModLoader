﻿using Mod_Uploader.ViewModels;

namespace Mod_Uploader.Abstractions;

public interface IViewModel
{
    ViewModelBase GetViewModel();
}