﻿using System.Threading.Tasks;

namespace Mod_Uploader.Abstractions;

public interface IHeaderViewModel : IViewModel
{
    void UpdateTitleText();
    Task FetchUsersItems();
}