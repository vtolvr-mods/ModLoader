﻿using System.Threading.Tasks;
using Facepunch.Steamworks.Abstractions;
using FluentAssertions;
using Mod_Uploader.Models;
using Moq;
using NUnit.Framework;

namespace Mod_Uploader.Tests.Models;

public class ContextTests
{
    [Category("Unit")]
    public class a_context
    {
        [Test, Timeout(600)]
        public async Task calls_event_when_scope_changed()
        {
            var context = new Context();
            var taskCompletionSource = new TaskCompletionSource<IItem>();

            context.SteamItemScopeChanged += (_, item) =>
            {
                taskCompletionSource.TrySetResult(item);
            };

            var mock = new Mock<IItem>();
            context.ChangeSteamItemScope(this, mock.Object);

            var item = await taskCompletionSource.Task;
            item.Should().BeEquivalentTo(mock.Object);

            context.GetSteamItemScope().Should().BeEquivalentTo(mock.Object);
        }
    }
}