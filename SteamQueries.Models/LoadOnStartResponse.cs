namespace SteamQueries.Models
{
    public class LoadOnStartResponse : IMessage
    {
        public string MessageType { get; set; } = nameof(LoadOnStartResponse);
        public LoadOnStart Data { get; set; }
    }
}