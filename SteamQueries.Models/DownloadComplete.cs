namespace SteamQueries.Models
{
    public class DownloadComplete : IMessage
    {
        public string MessageType { get; set; } = nameof(DownloadComplete);
        public ulong MessageId { get; set; }
    }
}