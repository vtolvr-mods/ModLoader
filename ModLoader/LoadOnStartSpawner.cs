using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cysharp.Threading.Tasks;
using Serilog;
using UnityEngine;

namespace ModLoader
{
    internal class LoadOnStartSpawner : MonoBehaviour
    {
        private SteamQuery.SteamQueries _steamQueries;
        private async UniTaskVoid Start()
        {
            Debug.Log($"Hello from {nameof(LoadOnStartSpawner)}");
            _steamQueries = GameObject.FindObjectOfType<SteamQuery.SteamQueries>();
            if (_steamQueries == null)
            {
                Log.Error("{Type} was unable to find {SteamQueries}", nameof(LoadOnStartSpawner), nameof(SteamQuery.SteamQueries));
                return;
            }
            
            await RequestForLoadOnStartSettings();
        }

        private async UniTask RequestForLoadOnStartSettings()
        {
            var settings = await _steamQueries.GetLoadOnStartSettings();

            if (settings == null)
            {
                Debug.LogError("Error! Load On Start request returned null");
                return;
            }
            
            await LoadLocalItems(settings.Data.LocalItems);
            await LoadSteamItems(settings.Data.WorkshopItems);
        }

        private async UniTask LoadLocalItems(Dictionary<string, bool> localItems)
        {
            var itemsToLoad = localItems.Where(item => item.Value)
                .ToDictionary(item => item.Key, item => item.Value);
            if (!itemsToLoad.Any())
            {
                Log.Information("Load On Start has no local items enabled");
                return;
            }

            var localItemsFound = ModLoader.Instance.FindLocalItems();

            foreach (var item in localItemsFound)
            {
                var name = Path.GetFileName(item.Directory);
                if (itemsToLoad.ContainsKey(name))
                {
                    await ModLoader.Instance.LoadSteamItem(item);
                }
            }
        }

        private async UniTask LoadSteamItems(Dictionary<ulong, bool> steamItems)
        {
            var itemsToLoad = steamItems.Where(item => item.Value)
                .ToDictionary(item => item.Key, item => item.Value);
            if (!itemsToLoad.Any())
            {
                Log.Information("Load On Start has no steam items enabled");
                return;
            }
            
            var currentPage = 1;
            const int maxPages = 100;
            
            while (true)
            {
                if (currentPage > maxPages)
                {
                    // Just stopping it if it goes too far
                    break;
                }
                
                var pageResults = await _steamQueries.GetSubscribedItems(currentPage);
                if (pageResults == null)
                {
                    break;
                }

                if (!pageResults.HasValues)
                {
                    Debug.LogWarning("Get Subscribed Items didn't have any values");
                    break;
                }

                var visibleItems = pageResults.Items.Where(item => item.MetaData is { AllowLoadOnStart: true }).ToArray();

                if (!visibleItems.Any())
                {
                    // No more subbed items
                    break;
                }

                for (int index = 0; index < visibleItems.Length; index++)
                {
                    var steamItem = visibleItems[index];

                    if (itemsToLoad.ContainsKey(steamItem.PublishFieldId))
                    {
                        await ModLoader.Instance.LoadSteamItem(steamItem);
                    }
                }

                currentPage++;
                await UniTask.WaitForEndOfFrame(this);
            }
        }
    }
}