﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Facepunch.Steamworks.Abstractions;
using Steamworks;

namespace Mod_Manager.Models;

public class Items
{
    private readonly IQuery _query;
    public Items(IQuery query)
    {
        _query = query;
    }
    
    public async Task<IEnumerable<Item>> GetSubscribedItemsAsync(int page)
    {
        var resultPage = await _query
            .WhereUserSubscribed()
            .WithMetadata(true)
            .WithDifferentApp(App.LoaderAppId, App.UploaderAppId)
            .GetPageAsync(page);
        
        var returnList = new List<Item>(resultPage.ResultCount);
        foreach (var newItem in resultPage.Entries)
        {
            returnList.Add(new Item(newItem));
        }

        return returnList;
    }

}