﻿using System.Threading.Tasks;
using Facepunch.Steamworks.Abstractions;

namespace Mod_Manager.Abstractions.VIewModel;

internal interface IHomeViewModel : IViewModel
{
    void ClearList();
    Task GetWorkshopItems(IQuery steamQuery);
    void GetLocalItems(string baseGameFolder);
    Task DownloadImages();
}