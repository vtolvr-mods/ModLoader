using CommandLine;

namespace Builder.Compile;

// ReSharper disable once ClassNeverInstantiated.Global
[Verb("compile", HelpText = "Compiles all the projects")]
class CompileCommandLine : BaseVerb
{
    
    [Option("dotnet", Required = true, HelpText = "Full path to DotNet.exe")]
    public string DotNetPath { get; set; }
    
    [Option("nuget", Required = true, HelpText = "Full path to nuget.exe")]
    public string NuGetPath { get; set; }
}