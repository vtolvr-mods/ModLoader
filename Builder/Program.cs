﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Builder.Compile;
using Builder.Extensions;
using Builder.Package;
using CommandLine;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Builder;

static class Program
{
    static async Task<int> Main(string[] args)
    {
        var builder = Host.CreateApplicationBuilder();
        Log.Logger = new LoggerConfiguration()
            .WriteTo.Console()
            .MinimumLevel.Information()
            .CreateLogger();

        builder.Services.AddTransient<CompileAction>();
        builder.Services.AddTransient<PackageAction>();
        builder.Services.AddSingleton<ILogger>(Log.Logger);
            
        var host = builder.Build();

        return await Parser.Default.ParseArguments<CompileCommandLine, PackageCommandLine>(args)
            .MapResult(
                (CompileCommandLine compile) => RunCompile(host, compile),
                (PackageCommandLine package) => RunPackage(host, package),
                errs =>
                {
                    Environment.Exit(1);
                    return Task.FromResult(1);
                });
    }

    private static async Task<int> RunCompile(IHost host, CompileCommandLine commandLine)
    {
        var action = host.Services.GetService<CompileAction>();
        var result = await action.DoAction(commandLine);
        host.Dispose();
        return result;
    }
        
    private static async Task<int> RunPackage(IHost host, PackageCommandLine commandLine)
    {
        var action = host.Services.GetService<PackageAction>();
        var result = await action.DoAction(commandLine);
        host.Dispose();
        return result;
    }
    
    public static async Task<int> Run(ILogger log, string file, string args, string workingDirectory, params string[] textToRedact)
    {
        var startInfo = new ProcessStartInfo
        {
            FileName = file, 
            Arguments = args, 
            UseShellExecute = false, 
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            WorkingDirectory = workingDirectory
        };
        var process = new Process
        {
            StartInfo = startInfo,
            EnableRaisingEvents = true
        };
        var processName = Path.GetFileName(file);
        process.ErrorDataReceived += (sender, eventArgs) =>
        {
            if (eventArgs.Data == null)
                return;
            log.Error("{ProcessName}:{ErrorText}", processName.Redacted(textToRedact), eventArgs.Data.Redacted(textToRedact));
        };

        process.OutputDataReceived += (sender, eventArgs) =>
        {
            if (eventArgs.Data == null)
                return;
            log.Information("{ProcessName}:{Text}", processName.Redacted(textToRedact), eventArgs.Data.Redacted(textToRedact));
        };
        log.Information("Starting {ProcessName} with arguments {Args} in {WorkingDirectory}",
            processName.Redacted(textToRedact),
            args.Redacted(textToRedact),
            workingDirectory.Redacted(textToRedact));
        process.Start();
        process.BeginOutputReadLine();
        process.BeginErrorReadLine();
        await process.WaitForExitAsync();
        return process.ExitCode;
    }
}