using CommandLine;

namespace Builder;

class BaseVerb
{
    [Option('p', "path", Required = true, HelpText = "Full path to the Mod Loader Repo")]
    public string ModLoaderRepo { get; set; }
}