using System.Linq;

namespace Builder.Extensions;

static class StringExtensions
{
    public static string Redacted(this string initialValue, params string[] textToRedact)
    {
        return textToRedact.Aggregate(initialValue, (current, text) => current.Replace(text, "REDACTED"));
    }
}