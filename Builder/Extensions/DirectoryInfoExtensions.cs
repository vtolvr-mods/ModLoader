using System.IO;

namespace Builder.Extensions;

static class DirectoryInfoExtensions
{
    public static void CopyAllFiles(this DirectoryInfo directory, string newLocation)
    {
        foreach (var fileInfo in directory.GetFiles())
        {
            fileInfo.CopyTo(Path.Combine(newLocation, fileInfo.Name));
        }
    }
}