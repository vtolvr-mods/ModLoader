using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Builder.Extensions;
using Octokit;
using Serilog;

namespace Builder.Package;

class PackageAction(ILogger logger)
{
    private string _appVdfTemplate = """
                                           "AppBuild"
                                           {
                                           	    "AppID" "%APPID%" // Your AppID
                                           	    "Desc" "%DESC%" // internal description for this build
                                           	    "Preview" "0" // make this a preview build only, nothing is uploaded
                                           	    "SetLive" "" // set this build live on beta branch AlphaTest
                                           	    "ContentRoot" "%CONTENTROOT%" // content root folder relative to this script file
                                           	    "BuildOutput" "%BUILDOUTPUT%" // put build cache and log files on different drive for better performance
                                           	    "Depots"
                                           	    {
                                           		    // file mapping instructions for each depot are in separate script files
                                           		    "%DEPOTID%" "depot_build.vdf"
                                           	    }
                                           }
                                           """;

    private const string _depotVdfTemplate = """
                                             "DepotBuild"
                                             {
                                             	// Set your assigned depot ID here
                                             	"DepotID" "%DEPOTID%"
                                             
                                             	// include all files recursivley
                                             	"FileMapping"
                                             	{
                                             		// This can be a full path, or a path relative to ContentRoot
                                             		"LocalPath" "*"
                                             
                                             		// This is a path relative to the install folder of your game
                                             		"DepotPath" "."
                                             		
                                             		// If LocalPath contains wildcards, setting this means that all
                                             		// matching files within subdirectories of LocalPath will also
                                             		// be included.
                                             		"Recursive" "1"
                                               }
                                             }
                                             """;
    public async Task<int> DoAction(PackageCommandLine commandLine)
    {
        if (!commandLine.SkipLoader)
        {
            await PackageModLoader(commandLine);
        }
        else
        {
            logger.Warning("Skipping packaging Mod Loader");
        }

        if (!commandLine.SkipModUploader)
        {
            await PackageModUploader(commandLine);
        }
        else
        {
            logger.Warning("Skipping packaging Mod Uploader");
        }
        
        return 0;
    }

    private async Task PackageModLoader(PackageCommandLine commandLine)
    {
        logger.Information("Packing up Mod Loader");
        var temp = Path.Combine(Environment.GetEnvironmentVariable("temp") ?? string.Empty, "Mod Loader Temp Package");
        var content = Path.Combine(temp, "Content");
        var buildOutput = Path.Combine(temp, "SteamCMD Logs");
        var downloads = Path.Combine(temp, "Downloads");

        var steamQueries = Path.Combine(content, "@Mod Loader", "SteamQueries");
        var managed = Path.Combine(content, "@Mod Loader", "Managed");
        var modManager = Path.Combine(content, "@Mod Loader", "Mod Manager");

        if (Directory.Exists(temp))
        {
            Directory.Delete(temp, true);
        }

        Directory.CreateDirectory(buildOutput);
        Directory.CreateDirectory(steamQueries);
        Directory.CreateDirectory(managed);
        Directory.CreateDirectory(modManager);
        Directory.CreateDirectory(downloads);
        
        var doorstopDownloadTask = DownloadDoorStop(downloads);
        
        var modManagerPublished = new DirectoryInfo(Path.Combine(commandLine.ModLoaderRepo, "Mod Manager","bin", "Release", "net6.0",
            "win-x64", "publish"));
        
        modManagerPublished.CopyAllFiles(modManager);
        
        var steamQueriesPublished = new DirectoryInfo(Path.Combine(commandLine.ModLoaderRepo, "SteamQueries","bin", "Release", "net7.0",
            "win-x64", "publish"));
        
        steamQueriesPublished.CopyAllFiles(steamQueries);

        var managedFilesNeeded = new string[]
        {
            "0Harmony.dll", "JsonSubTypes.dll", "JsonSubTypes.pdb", "Mod Loader.deps.json", "Mod Loader.dll",
            "Mod Loader.dll.config", "Mod Loader.pdb", "ModLoader.Framework.dll", "ModLoader.Framework.pdb",
            "Mono.Cecil.dll", "Mono.Cecil.Mdb.dll", "Mono.Cecil.Pdb.dll", "Mono.Cecil.Rocks.dll",
            "MonoMod.Backports.dll", "MonoMod.Core.dll", "MonoMod.Iced.dll", "MonoMod.ILHelpers.dll",
            "MonoMod.RuntimeDetour.dll", "MonoMod.Utils.dll", "Serilog.dll", "Serilog.Sinks.File.dll",
            "SimpleTCP.dll", "SteamQueries.Models.dll", "SteamQueries.Models.pdb",
            "System.Diagnostics.DiagnosticSource.dll", "System.Numerics.Vectors.dll", "System.Reflection.Emit.dll",
            "System.Reflection.Emit.ILGeneration.dll", "System.Reflection.Emit.Lightweight.dll",
            "System.Threading.Channels.dll", "System.Threading.Tasks.Extensions.dll", "System.Text.Encodings.Web.dll", 
            "System.Text.Json.dll", "Microsoft.Bcl.AsyncInterfaces.dll"
        };

        var modLoaderBuild = new DirectoryInfo(Path.Combine(commandLine.ModLoaderRepo, "ModLoader", "bin", "Release", "netstandard2.0", "publish"));

        foreach (var file in modLoaderBuild.GetFiles())
        {
            if (!managedFilesNeeded.Contains(file.Name))
            {
                continue;
            }

            file.CopyTo(Path.Combine(managed, file.Name));
        }

        var assetsBundle = new FileInfo(Path.Combine(commandLine.ModLoaderRepo, "Asset Bundle Project", "Assets", "@Mod Loader",
            "Output", "assets"));

        assetsBundle.CopyTo(Path.Combine(content, "@Mod Loader", assetsBundle.Name));
        
        logger.Information("Finished copying files to content folder");

        // The file is 22kb, surely won't take longer than 2 minutes
        await doorstopDownloadTask.WaitAsync(TimeSpan.FromMinutes(2));

        var expectedDoorstop = new DirectoryInfo(Path.Combine(downloads, "x64"));
        var unwantedFile = new FileInfo(Path.Combine(expectedDoorstop.FullName, "doorstop_config.ini"));
        unwantedFile.Delete();
        expectedDoorstop.CopyAllFiles(content);
        
        var buildOutputFolder = Path.Combine(temp, "SteamCMD Logs");
        Directory.CreateDirectory(buildOutputFolder);
        
        var appFilePath = await CreateAppFile(temp, "3018410", "3018411", content, buildOutputFolder, commandLine.SteamUploadDescription);
        
        if (commandLine.SkipUploading)
        {
            logger.Warning("Skipping uploading 'Mod Loader' to steam");
            logger.Information("You can view the content at {ContentFolder}", content);
            return;
        }

        await Program.Run(logger, commandLine.SteamCmd,
            $"+login {commandLine.SteamUserName} +run_app_build \"{appFilePath}\" +quit",
            string.Empty);
        logger.Information("Finished Steam Upload!");
    }

    private async Task PackageModUploader(PackageCommandLine commandLine)
    {
        logger.Information("Packing up Mod Uploader");
        var temp = Path.Combine(Environment.GetEnvironmentVariable("temp") ?? string.Empty, "Mod Uploader Temp Package");

        if (Directory.Exists(temp))
        {
            Directory.Delete(temp, true);
        }

        var dir = Directory.CreateDirectory(Path.Combine(temp, "Content", "@Mod Loader", "Mod Uploader"));

        var publishedPath = Path.Combine(commandLine.ModLoaderRepo, "Mod Uploader", "bin", "Release", "net6.0",
            "win-x64", "publish");

        var publishedDirectory = new DirectoryInfo(publishedPath);
        
        publishedDirectory.CopyAllFiles(dir.FullName);
        
        var contentFolder = Path.Combine(temp, "Content");

        var buildOutputFolder = Path.Combine(temp, "SteamCMD Logs");
        Directory.CreateDirectory(buildOutputFolder);

        var appFilePath = await CreateAppFile(temp, "3018440", "3018441", contentFolder, buildOutputFolder, commandLine.SteamUploadDescription);
        
        if (commandLine.SkipUploading)
        {
            logger.Warning("Skipping uploading 'Mod Uploader' to steam");
            logger.Information("You can view the content at {ContentFolder}", contentFolder);
            return;
        }

        await Program.Run(logger, commandLine.SteamCmd,
            $"+login {commandLine.SteamUserName} +run_app_build \"{appFilePath}\" +quit",
            string.Empty);
        logger.Information("Finished Steam Upload!");
    }

    private async Task<string> CreateAppFile(string folder, string appId, string depotId, string contentFolder, string buildOutput, string description)
    {
        var appContents = _appVdfTemplate
            .Replace("%APPID%", appId)
            .Replace("%CONTENTROOT%", contentFolder)
            .Replace("%BUILDOUTPUT%", buildOutput)
            .Replace("%DEPOTID%", depotId)
            .Replace("%DESC%", description);

        var returnedPath = Path.Combine(folder, "app_build.vdf");
        await File.WriteAllTextAsync(returnedPath, appContents);

        var depotContents = _depotVdfTemplate.Replace("%DEPOTID%", depotId);

        await File.WriteAllTextAsync(Path.Combine(folder, "depot_build.vdf"), depotContents);

        return returnedPath;
    }

    private async Task DownloadDoorStop(string folder)
    {
        logger.Information("Started downloading doorstop task");
        var github = new GitHubClient(new ProductHeaderValue("vtolvr-mods"));
        var latest = await github.Repository.Release.GetLatest("NeighTools", "UnityDoorstop");

        var windowsRelease = latest.Assets.First(r =>
            r.Name.StartsWith("doorstop_win_release", StringComparison.OrdinalIgnoreCase));

        using var client = new HttpClient();
        var fileName = Path.Combine(folder, windowsRelease.Name);
        logger.Information("Started downloading doorstop release zip");
        await client.DownloadFile(windowsRelease.BrowserDownloadUrl, fileName);
        ZipFile.ExtractToDirectory(fileName, folder);
        logger.Information("Finished downloading doorstop release and extracted it");
    }
}