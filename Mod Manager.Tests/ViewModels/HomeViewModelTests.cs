using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using Facepunch.Steamworks.Abstractions;
using Facepunch.Steamworks.Abstractions.Wrappers;
using Material.Icons;
using Mod_Manager.Abstractions;
using Mod_Manager.ViewModels;
using Newtonsoft.Json;
using SteamQueries.Models;
using Steamworks;

namespace Mod_Manager.Tests.ViewModels;

public class HomeViewModelTests
{
    public const string BaseGameFolder = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
    public const string LocalItemsFolderName = @"@Mod Loader\Mods";
    public const string LocalItemFile = "item.json";
    [Category("Unit")]
    class a_home_view_model
    {
        [Test]
        public async Task when_downloading_images_calls_http()
        {
            var loadOnStartManager = new Mock<ILoadOnStartManager>();
            loadOnStartManager
                .Setup(los => los.GetLoadOnStartItems())
                .Returns(new LoadOnStart());
                
                
            var steamItem = new Mock<IItem>();
            steamItem
                .Setup(i => i.Id.Value)
                .Returns(123456789);
            steamItem
                .Setup(i => i.NumSubscriptions)
                .Returns(1);
            steamItem
                .Setup(i => i.OwnerName)
                .Returns("Test Owner");
            steamItem
                .Setup(i => i.PreviewImageUrl)
                .Returns("http://example.com/image.png");
            steamItem
                .Setup(i => i.Title)
                .Returns("Test Item");

            var query = new Mock<IQuery>();
            query
                .Setup(q => q.WhereUserSubscribed())
                .Returns(query.Object);
            query
                .Setup(q => q.WithDifferentApp(It.IsAny<AppId>(), It.IsAny<AppId>()))
                .Returns(query.Object);
            query
                .Setup(q => q.GetPageAsync(It.IsAny<int>()))
                .ReturnsAsync(new ResultsPage(1, new IItem[]
                {
                    steamItem.Object
                }));
            query.Setup(q => q.WithMetadata(It.IsAny<bool>())).Returns(query.Object);

            var http = new Mock<IHttp>();
            var view = new HomeViewModel(http.Object, loadOnStartManager.Object, Mock.Of<IFileSystem>());
            await view.GetWorkshopItems(query.Object);
            await view.DownloadImages();
            
            http.Verify(h => h.GetImageAsync(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void get_view_model_returns_type_of_view_model()
        {
            var view = new HomeViewModel(Mock.Of<IHttp>(), Mock.Of<ILoadOnStartManager>(), Mock.Of<IFileSystem>());
            var inheritance = view.GetViewModel() is ViewModelBase;
            inheritance.Should().BeTrue();
        }
        
        [Category("Unit")]
        class when_getting_workshop_items
        {
            [Test]
            public async Task requests_for_load_on_start_items()
            {
                var loadOnStartManager = new Mock<ILoadOnStartManager>();
                loadOnStartManager
                    .Setup(los => los.GetLoadOnStartItems())
                    .Returns(new LoadOnStart());
                
                
                var steamItem = new Mock<IItem>();
                steamItem
                    .Setup(i => i.Id.Value)
                    .Returns(123456789);
                steamItem
                    .Setup(i => i.NumSubscriptions)
                    .Returns(1);
                steamItem
                    .Setup(i => i.OwnerName)
                    .Returns("Test Owner");
                steamItem
                    .Setup(i => i.PreviewImageUrl)
                    .Returns("http://example.com/image.png");
                steamItem
                    .Setup(i => i.Title)
                    .Returns("Test Item");

                var query = new Mock<IQuery>();
                query
                    .Setup(q => q.WhereUserSubscribed())
                    .Returns(query.Object);
                query
                    .Setup(q => q.WithDifferentApp(It.IsAny<AppId>(), It.IsAny<AppId>()))
                    .Returns(query.Object);
                query
                    .Setup(q => q.GetPageAsync(It.IsAny<int>()))
                    .ReturnsAsync(new ResultsPage(1, new IItem[]
                    {
                        steamItem.Object
                    }));
                query.Setup(q => q.WithMetadata(It.IsAny<bool>())).Returns(query.Object);
                
                var view = new HomeViewModel(Mock.Of<IHttp>(), loadOnStartManager.Object, Mock.Of<IFileSystem>());
                await view.GetWorkshopItems(query.Object);
                
                loadOnStartManager.Verify(los => los.GetLoadOnStartItems(), Times.Once);
            }
            
            [Test]
            public async Task adds_to_my_items()
            {
                const ulong publicId = 123456789;
                
                var loadOnStartManager = new Mock<ILoadOnStartManager>();
                loadOnStartManager
                    .Setup(los => los.GetLoadOnStartItems())
                    .Returns(new LoadOnStart
                    {
                        WorkshopItems = new Dictionary<ulong, bool>
                        {
                            {publicId, true}
                        }
                    });
                
                
                var steamItem = new Mock<IItem>();
                steamItem
                    .Setup(i => i.Id.Value)
                    .Returns(publicId);
                steamItem
                    .Setup(i => i.NumSubscriptions)
                    .Returns(1);
                steamItem
                    .Setup(i => i.OwnerName)
                    .Returns("Test Owner");
                steamItem
                    .Setup(i => i.PreviewImageUrl)
                    .Returns("http://example.com/image.png");
                steamItem
                    .Setup(i => i.Title)
                    .Returns("Test Item");
                steamItem.Setup(i => i.MetaData)
                    .Returns(JsonConvert.SerializeObject(new ItemMetaData
                    {
                        AllowLoadOnStart = true,
                        ShowOnMainList = true,
                    }));

                var query = new Mock<IQuery>();
                query
                    .Setup(q => q.WhereUserSubscribed())
                    .Returns(query.Object);
                query
                    .Setup(q => q.WithDifferentApp(It.IsAny<AppId>(), It.IsAny<AppId>()))
                    .Returns(query.Object);
                query
                    .Setup(q => q.GetPageAsync(It.IsAny<int>()))
                    .ReturnsAsync(new ResultsPage(1, new IItem[]
                    {
                        steamItem.Object
                    }));
                query.Setup(q => q.WithMetadata(It.IsAny<bool>())).Returns(query.Object);
                
                var view = new HomeViewModel(Mock.Of<IHttp>(), loadOnStartManager.Object, Mock.Of<IFileSystem>());
                await view.GetWorkshopItems(query.Object);

                view.MyItems.Count.Should().Be(1);
                view.MyItems.First().LosIcon.Should().Be(MaterialIconKind.RocketLaunch);
            }

            [Test]
            public async Task clears_list_when_getting_new_items()
            {
                const ulong publicId = 123456789;

                var loadOnStartManager = new Mock<ILoadOnStartManager>();
                loadOnStartManager
                    .Setup(los => los.GetLoadOnStartItems())
                    .Returns(new LoadOnStart { WorkshopItems = new Dictionary<ulong, bool> { { publicId, true } } });


                var steamItem = new Mock<IItem>();
                steamItem
                    .Setup(i => i.Id.Value)
                    .Returns(publicId);
                steamItem
                    .Setup(i => i.NumSubscriptions)
                    .Returns(1);
                steamItem
                    .Setup(i => i.OwnerName)
                    .Returns("Test Owner");
                steamItem
                    .Setup(i => i.PreviewImageUrl)
                    .Returns("http://example.com/image.png");
                steamItem
                    .Setup(i => i.Title)
                    .Returns("Test Item");
                steamItem.Setup(i => i.MetaData)
                    .Returns(JsonConvert.SerializeObject(new ItemMetaData
                    {
                        AllowLoadOnStart = true, ShowOnMainList = true,
                    }));

                var query = new Mock<IQuery>();
                query
                    .Setup(q => q.WhereUserSubscribed())
                    .Returns(query.Object);
                query
                    .Setup(q => q.WithDifferentApp(It.IsAny<AppId>(), It.IsAny<AppId>()))
                    .Returns(query.Object);
                query
                    .Setup(q => q.GetPageAsync(It.IsAny<int>()))
                    .ReturnsAsync(new ResultsPage(1, new IItem[] { steamItem.Object }));
                query.Setup(q => q.WithMetadata(It.IsAny<bool>())).Returns(query.Object);

                var view = new HomeViewModel(Mock.Of<IHttp>(), loadOnStartManager.Object, Mock.Of<IFileSystem>());
                await view.GetWorkshopItems(query.Object);
                view.MyItems.Count.Should().Be(1);
                view.ClearList();
                await view.GetWorkshopItems(query.Object);
                view.MyItems.Count.Should().Be(1);

            }
        }
        
        [Category("Unit")]
        class when_getting_local_items
        {
            [Test]
            public void does_not_throw_if_folder_does_not_exist()
            {
                var fileSystem = new MockFileSystem();
                var view = new HomeViewModel(Mock.Of<IHttp>(), Mock.Of<ILoadOnStartManager>(), fileSystem);
                view.Invoking(v => v.GetLocalItems(BaseGameFolder)).Should().NotThrow();
            }
            
            [Test]
            public void does_not_throw_if_sub_folders_does_not_exist()
            {
                var fileSystem = new MockFileSystem();
                fileSystem.AddDirectory(Path.Combine(BaseGameFolder, LocalItemsFolderName));
                
                var view = new HomeViewModel(Mock.Of<IHttp>(), Mock.Of<ILoadOnStartManager>(), fileSystem);
                view.Invoking(v => v.GetLocalItems(BaseGameFolder)).Should().NotThrow();
            }

            [Test]
            public void does_not_add_items_if_file_is_missing()
            {
                var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
                {
                    { Path.Combine(BaseGameFolder, LocalItemsFolderName, "Test Item", "TestItem.dll"), new MockFileData(string.Empty)}
                });
                
                var view = new HomeViewModel(Mock.Of<IHttp>(), Mock.Of<ILoadOnStartManager>(), fileSystem);
                view.GetLocalItems(BaseGameFolder);
                view.MyItems.Count.Should().Be(0);
            }
            
            [Test]
            public void does_not_add_items_if_file_is_invalid()
            {
                var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
                {
                    { Path.Combine(BaseGameFolder, LocalItemsFolderName, "Test Item", LocalItemFile), new MockFileData("invalidJson")}
                });
                
                var view = new HomeViewModel(Mock.Of<IHttp>(), Mock.Of<ILoadOnStartManager>(), fileSystem);
                view.GetLocalItems(BaseGameFolder);
                view.MyItems.Count.Should().Be(0);
            }
            
            [Test]
            public void checks_load_on_start_manager()
            {
                var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
                {
                    { Path.Combine(BaseGameFolder, LocalItemsFolderName, "Test Item", LocalItemFile), 
                        new MockFileData(JsonConvert.SerializeObject(new SteamItem()))}
                });
                
                var loadOnStartManager = new Mock<ILoadOnStartManager>();
                loadOnStartManager
                    .Setup(los => los.GetLoadOnStartItems())
                    .Returns(new LoadOnStart());
                
                var view = new HomeViewModel(Mock.Of<IHttp>(), loadOnStartManager.Object, fileSystem);
                view.GetLocalItems(BaseGameFolder);
                loadOnStartManager
                    .Verify(los => los.GetLoadOnStartItems(), Times.Once);
            }
            
            [Test]
            public void adds_items()
            {
                const string folderName = "Test Item";
                var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
                {
                    { Path.Combine(BaseGameFolder, LocalItemsFolderName, folderName, LocalItemFile), 
                        new MockFileData(JsonConvert.SerializeObject(new SteamItem()))}
                });
                
                var loadOnStartManager = new Mock<ILoadOnStartManager>();
                loadOnStartManager
                    .Setup(los => los.GetLoadOnStartItems())
                    .Returns(new LoadOnStart
                    {
                        LocalItems = new Dictionary<string, bool>
                        {
                            {folderName, true}
                        }
                    });
                
                var view = new HomeViewModel(Mock.Of<IHttp>(), loadOnStartManager.Object, fileSystem);
                view.GetLocalItems(BaseGameFolder);
                view.MyItems.Count.Should().Be(1);
                view.MyItems.First().LosIcon.Should().Be(MaterialIconKind.RocketLaunch);
            }
        }
    }
}