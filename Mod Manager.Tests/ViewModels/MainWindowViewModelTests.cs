﻿using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.VIewModel;
using Mod_Manager.ViewModels;

namespace Mod_Manager.Tests.ViewModels;

public class MainWindowViewModelTests
{
    class main_view_model
    {
        [Test]
        public void returns_view_models()
        {
            var view = new MainWindowViewModel(Mock.Of<IHeaderBarViewModel>(), Mock.Of<IHomeViewModel>(), Mock.Of<IPopUpView>());
            var doesInherit = view.GetViewModel() is ViewModelBase;
            doesInherit.Should().BeTrue();
        }

        [Test]
        public void invokes_get_view_model_from_getters()
        {
            var homeView = new Mock<IHomeViewModel>();
            var headerView = new Mock<IHeaderBarViewModel>();
            var popUpView = new Mock<IPopUpView>();
            var view = new MainWindowViewModel(headerView.Object,
                homeView.Object, popUpView.Object);

            var getting = view.MainView;
            homeView.Verify(view => view.GetViewModel(), Times.Once);
            
            getting = view.HeaderBarView;
            headerView.Verify(view => view.GetViewModel(), Times.Once);

            getting = view.PopUpView;
            popUpView.Verify(view => view.GetViewModel(), Times.Once);
        }
    }
}