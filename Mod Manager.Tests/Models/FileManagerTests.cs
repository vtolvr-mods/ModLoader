﻿using System.IO.Abstractions.TestingHelpers;
using Mod_Manager.Models;

namespace Mod_Manager.Tests.Models;

public class FileManagerTests
{

    [Test]
    public void returns_parent_directory_when_getting_vtol_directory()
    {
        const string currentDir = @"D:\Program Files (x86)\Steam\steamapps\common\VTOL VR\@Mod Loader\Mod Manager";
        const string vtolDir = @"D:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
            
        var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
        {
            { currentDir, new MockDirectoryData() },
            { vtolDir, new MockDirectoryData() }
        });
        fileSystem.Directory.SetCurrentDirectory(currentDir);
            
        var fileManager = new FileManager(fileSystem);
        fileManager.GetVtolDirectory().Should().BeEquivalentTo(vtolDir);
    }
}