﻿using System.IO.Abstractions.TestingHelpers;
using Mod_Manager.Abstractions;
using Mod_Manager.Models;

namespace Mod_Manager.Tests.Models;

public class InitTests
{
    [Test]
    public void can_find_vtol_in_parent_folder()
    {
        const string fakeVtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
        const string fakeCurrentDir = @$"{fakeVtolDir}\@Mod Loader\Mod Manager";
        const string vtolExeName = fakeVtolDir + @"\VTOLVR.exe";
        const string dataFolderName = fakeVtolDir + @"\VTOLVR_Data";
            
        var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
        {
            { vtolExeName, new MockFileData(String.Empty) },
            { dataFolderName, new MockDirectoryData() },
            { fakeCurrentDir, new MockDirectoryData() }
                
        });
        fileSystem.Directory.SetCurrentDirectory(fakeCurrentDir);

        var init = new Init(fileSystem);
        init.IsInGameFiles().Should().BeTrue();
    }

    [Test]
    public void can_fail_finding_vtol_in_parent_folder()
    {        
        const string fakeVtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
        const string fakeCurrentDir = @$"{fakeVtolDir}\@Mod Loader\Mod Manager";
        const string dataFolderName = fakeVtolDir + @"\VTOLVR_Data";
        const string saveDataFolderName = fakeVtolDir + @"\SaveData";
        const string radioMusicFolderName = fakeVtolDir + @"\RadioMusic";
            
        var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
        {
            // Missing "VTOL VR.exe"
            { dataFolderName, new MockDirectoryData() },
            { fakeCurrentDir, new MockDirectoryData() },
            { saveDataFolderName, new MockDirectoryData() },
            { radioMusicFolderName, new MockDirectoryData() },
                
        });
        fileSystem.Directory.SetCurrentDirectory(fakeCurrentDir);

        var init = new Init(fileSystem);
        init.IsInGameFiles().Should().BeFalse(); 
    }

    [Test]
    public void can_fail_finding_vtol_not_in_same_drive()
    {
        const string fakeCurrentDir = @"D:\Program Files (x86)\Steam\steamapps\common\VTOL VR\@Mod Loader\Mod Manager";          
        const string fakeVtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
        const string dataFolderName = fakeVtolDir + @"\VTOLVR_Data";
        const string vtolExeName = fakeVtolDir + @"\VTOLVR.exe";
        const string saveDataFolderName = fakeVtolDir + @"\SaveData";
        const string radioMusicFolderName = fakeVtolDir + @"\RadioMusic";
            
        var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
        {
            { dataFolderName, new MockDirectoryData() },
            { vtolExeName, new MockFileData(String.Empty) },
            { fakeCurrentDir, new MockDirectoryData() },
            { saveDataFolderName, new MockDirectoryData() },
            { radioMusicFolderName, new MockDirectoryData() },
                
        });
        fileSystem.Directory.SetCurrentDirectory(fakeCurrentDir);
        
        var init = new Init(fileSystem);
        init.IsInGameFiles().Should().BeFalse(); 
    }
    
    [Category("Unit")]
    private sealed class when_getting_workshop_folder
    {
        [Test]
        public void can_find_workshop_folder()
        {
            const string fakeVtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
            const string fakeCurrentDir = @$"{fakeVtolDir}\@Mod Loader\Mod Manager";
            const string vtolExeName = fakeVtolDir + @"\VTOLVR.exe";
            const string dataFolderName = fakeVtolDir + @"\VTOLVR_Data";
            const string workshopFolder =  @"C:\Program Files (x86)\Steam\steamapps\workshop\content\3018410";
            
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
            {
                { vtolExeName, new MockFileData(String.Empty) },
                { dataFolderName, new MockDirectoryData() },
                { fakeCurrentDir, new MockDirectoryData() },
                { workshopFolder, new MockDirectoryData() }
                
            });
            fileSystem.Directory.SetCurrentDirectory(fakeCurrentDir);

            var init = new Init(fileSystem);
            init.GetWorkshopFolder().Should().NotBeNullOrEmpty();
        }
        
        [Test]
        public void returns_empty_string_if_no_vtolvr_folder_found()
        {
            // This is missing VTOLVR.exe and VTOLVR_Data.
            // What it would be like if the user installed the mod loader on the wrong drive
            const string fakeVtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
            const string fakeCurrentDir = @$"{fakeVtolDir}\@Mod Loader\Mod Manager";
            const string workshopFolder =  @"C:\Program Files (x86)\Steam\steamapps\workshop\content\3018410";
            
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { fakeCurrentDir, new MockDirectoryData() },
                { workshopFolder, new MockDirectoryData() }
                
            });
            fileSystem.Directory.SetCurrentDirectory(fakeCurrentDir);

            var init = new Init(fileSystem);
            init.GetWorkshopFolder().Should().BeEmpty();
        }
    }

    
}