﻿using Facepunch.Steamworks.Abstractions;
using Mod_Manager.Models;
using Newtonsoft.Json;
using SteamQueries.Models;

namespace Mod_Manager.Tests.Models;

public class LoadOnStartManagerTests
{
    [Category("Unit")]
    class a_load_on_start_manager
    {
        [Test]
        public void if_file_does_not_exists_returns_empty_object()
        {
            var steamStorage = new Mock<ISteamRemoteStorage>();
            steamStorage.Setup(s => s.FileExists(It.IsAny<string>()))
                .Returns(false);

            var manager = new LoadOnStartManager(steamStorage.Object);
            manager.GetLoadOnStartItems().Should().BeEquivalentTo(new LoadOnStart());
        }

        [Test]
        public void can_return_object()
        {
            var expectedObject = new LoadOnStart
            {
                WorkshopItems = new Dictionary<ulong, bool>() { { 123, true } }
            };
            var json = JsonConvert.SerializeObject(expectedObject);
            var steamStorage = new Mock<ISteamRemoteStorage>();
            steamStorage.Setup(s => s.FileExists(It.IsAny<string>()))
                .Returns(true);
            steamStorage.Setup(s => s.FileReadText(It.IsAny<string>()))
                .Returns(json);
            
            var manager = new LoadOnStartManager(steamStorage.Object);
            manager.GetLoadOnStartItems().Should().BeEquivalentTo(expectedObject);
        }

        [Test]
        public void can_change_existing_values_without_throwing()
        {
            var expectedObject = new LoadOnStart
            {
                WorkshopItems = new Dictionary<ulong, bool>() { { 123, true } }
            };
            var json = JsonConvert.SerializeObject(expectedObject);
            var steamStorage = new Mock<ISteamRemoteStorage>();
            steamStorage.Setup(s => s.FileExists(It.IsAny<string>()))
                .Returns(true);
            steamStorage.Setup(s => s.FileReadText(It.IsAny<string>()))
                .Returns(json);
            
            var manager = new LoadOnStartManager(steamStorage.Object);
            manager.Invoking(m => m.ChangeStateOnItem(123, false)).Should().NotThrow();
            manager.Invoking(m => m.ChangeStateOnItem("MyMod", false)).Should().NotThrow();
            
            steamStorage
                .Verify(s => s.FileWrite(LoadOnStartManager.LoadOnStartFile, It.IsAny<string>()), 
                    Times.Exactly(2));
        }
    }
}